package ru.nirinarkhova.tm.exception.entity;

import ru.nirinarkhova.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User not found!");
    }

}
