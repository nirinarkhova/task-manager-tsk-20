package ru.nirinarkhova.tm.command.authorization;

import ru.nirinarkhova.tm.command.AbstractCommand;
import ru.nirinarkhova.tm.model.User;
import ru.nirinarkhova.tm.util.TerminalUtil;

public class UserPasswordChangeCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "change-password";
    }

    @Override
    public String description() {
        return "change password for current user";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("[ENTER NEW PASSWORD:]");
        final String newPassword = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().setPassword(userId, newPassword);
    }

}
