package ru.nirinarkhova.tm.api;

import ru.nirinarkhova.tm.model.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll(final String userId);

    E add(String userId, E entity);

    E findById(String userId, String id);

    void clear(String userId);

    E removeById(String userId, String id);

    void remove(String userId, E entity);

}
