package ru.nirinarkhova.tm.api.entity;

import java.util.Date;

public interface IHasDateStart {

    Date getDateStart();

    void setDateStart(Date date);

}
