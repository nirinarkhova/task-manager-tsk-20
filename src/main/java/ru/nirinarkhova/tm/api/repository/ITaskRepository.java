package ru.nirinarkhova.tm.api.repository;

import ru.nirinarkhova.tm.api.IRepository;
import ru.nirinarkhova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    Task findOneByProjectId(String userId, String projectId);

    List<Task> findAllByProjectId(String userId, String projectId);

    void removeAllByProjectId(String userId, String projectId);

    Task bindTaskByProject(String userId, String taskId, String projectId);

    Task unbindTaskByProject(String userId, String projectId);

    List<Task> findAll(Comparator<Task> comparator);

    Task findOneByName(String userId, String name);

    Task findOneByIndex(String userId, Integer index);

    Task removeOneByIndex(String userId, Integer index);

    Task removeOneByName(String userId, String name);

}
