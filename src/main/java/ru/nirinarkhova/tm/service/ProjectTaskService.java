package ru.nirinarkhova.tm.service;

import ru.nirinarkhova.tm.api.repository.IProjectRepository;
import ru.nirinarkhova.tm.api.service.IProjectTaskService;
import ru.nirinarkhova.tm.api.repository.ITaskRepository;
import ru.nirinarkhova.tm.exception.empty.EmptyUserIdException;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(final ITaskRepository taskRepository, final IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findTaskByProjectId(final  String userId, final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    public Task bindTaskByProject(final  String userId, String taskId, String projectId) {
        if (userId == null) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        final Task task = taskRepository.findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.bindTaskByProject(userId, taskId, projectId);
    }

    @Override
    public Task unbindTaskFromProject(final  String userId, String taskId) {
        if (userId == null) throw new EmptyUserIdException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        final Task task = taskRepository.findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.unbindTaskByProject(userId, taskId);
    }

    @Override
    public Project removeProjectById(final  String userId, String projectId) {
        if (userId == null) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        taskRepository.removeAllByProjectId(userId, projectId);
        return projectRepository.removeById(userId, projectId);
    }

}
