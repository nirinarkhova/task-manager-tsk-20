package ru.nirinarkhova.tm.model;

import ru.nirinarkhova.tm.api.entity.IWBS;
import ru.nirinarkhova.tm.enumerated.Status;

import java.util.Date;

public class Task extends AbstractEntity implements IWBS {

    private String projectId;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }


}
