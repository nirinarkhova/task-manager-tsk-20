package ru.nirinarkhova.tm.enumerated;

import ru.nirinarkhova.tm.comparator.ComparatorByCreated;
import ru.nirinarkhova.tm.comparator.ComparatorByDateStart;
import ru.nirinarkhova.tm.comparator.ComparatorByName;
import ru.nirinarkhova.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    CREATED("Sort by created date", ComparatorByCreated.getInstance()),
    DATE_START("Sort by start date", ComparatorByDateStart.getInstance()),
    NAME("Sort by name", ComparatorByName.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    private final String displayName;

    private final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public Comparator getComparator() {
        return comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

}
