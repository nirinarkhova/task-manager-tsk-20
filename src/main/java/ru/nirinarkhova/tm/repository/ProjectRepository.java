package ru.nirinarkhova.tm.repository;

import ru.nirinarkhova.tm.api.repository.IProjectRepository;
import ru.nirinarkhova.tm.exception.empty.EmptyUserIdException;
import ru.nirinarkhova.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public List<Project> findAll(Comparator<Project> comparator) {
        final List<Project> projects = new ArrayList<>(entities);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public Project findOneByIndex(final  String userId, final Integer index) {
        if (userId == null) throw new EmptyUserIdException();
        return entities.get(index);
    }

    @Override
    public Project findOneByName(final  String userId, final String name) {
        if (userId == null) throw new EmptyUserIdException();
        for (final Project project: entities) {
            if (!userId.equals(project.getUserId())) continue;
            if(name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneByIndex(final  String userId, final Integer index) {
        if (userId == null) throw new EmptyUserIdException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Override
    public Project removeOneByName(final  String userId, final String name) {
        if (userId == null) throw new EmptyUserIdException();
        final Project project = findOneByName(userId, name);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

}

