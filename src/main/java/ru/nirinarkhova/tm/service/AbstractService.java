package ru.nirinarkhova.tm.service;

import ru.nirinarkhova.tm.api.IRepository;
import ru.nirinarkhova.tm.api.IService;
import ru.nirinarkhova.tm.exception.empty.EmptyIdException;
import ru.nirinarkhova.tm.exception.empty.EmptyUserIdException;
import ru.nirinarkhova.tm.exception.entity.ObjectNotFoundException;
import ru.nirinarkhova.tm.model.AbstractEntity;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private final IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public List<E> findAll(final String userId) {
        return repository.findAll(userId);
    }

    @Override
    public E add(final String userId, final E entity) {
        if (userId == null) throw new EmptyUserIdException();
        if (entity == null) throw new ObjectNotFoundException();
        return repository.add(userId, entity);
    }

    @Override
    public E findById(final String userId, final String id) {
        if (userId == null) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null) throw new EmptyUserIdException();
        repository.clear(userId);
    }

    @Override
    public E removeById(final String userId, final String id) {
        if (userId == null) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(userId, id);
    }

    @Override
    public void remove(final String userId, final E entity) {
        if (userId == null) throw new EmptyUserIdException();
        if (entity == null) throw new ObjectNotFoundException();
        repository.remove(userId, entity);
    }

}

