package ru.nirinarkhova.tm.service;

import ru.nirinarkhova.tm.api.repository.ITaskRepository;
import ru.nirinarkhova.tm.api.service.ITaskService;
import ru.nirinarkhova.tm.enumerated.Status;
import ru.nirinarkhova.tm.exception.empty.EmptyIdException;
import ru.nirinarkhova.tm.exception.empty.EmptyIndexException;
import ru.nirinarkhova.tm.exception.empty.EmptyNameException;
import ru.nirinarkhova.tm.exception.empty.EmptyUserIdException;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return null;
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task add(final  String userId, final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
        return task;
    }

    @Override
    public Task findOneByName(final  String userId, final String name) {
        if (name == null) throw new EmptyNameException();
        return taskRepository.findOneByName(userId, name);
    }

    @Override
    public Task removeOneByName(final  String userId, final String name) {
        if (userId == null) throw new EmptyUserIdException();
        if (name == null) throw new EmptyNameException();
        return taskRepository.removeOneByName(userId, name);
    }

    @Override
    public Task findOneByIndex(final  String userId, final Integer index) {
        if (userId == null) throw new EmptyUserIdException();
        if (index == null) throw new EmptyIndexException();
        return taskRepository.findOneByIndex(userId, index);
    }

    @Override
    public Task removeOneByIndex(final  String userId, final Integer index) {
        if (userId == null) throw new EmptyUserIdException();
        if (index == null) throw new EmptyIndexException();
        return taskRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Task updateTaskByIndex(final  String userId, final Integer index, String name, String description) {
        if (userId == null) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskById(final  String userId, final String id, String name, String description) {
        if (userId == null) throw new EmptyUserIdException();
        if (id == null) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskById(final  String userId, String id) {
        if (userId == null) throw new EmptyUserIdException();
        if (id == null) throw new EmptyIdException();
        final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByName(final  String userId, String name) {
        if (userId == null) throw new EmptyUserIdException();
        if (name == null) throw new EmptyNameException();
        final Task task = findOneByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(final  String userId, Integer index) {
        if (userId == null) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishTaskById(final  String userId, String id) {
        if (userId == null) throw new EmptyUserIdException();
        if (id == null) throw new EmptyIdException();
        final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByName(final  String userId, String name) {
        if (userId == null) throw new EmptyUserIdException();
        if (name == null) throw new EmptyNameException();
        final Task task = findOneByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByIndex(final  String userId, Integer index) {
        if (userId == null) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final  String userId, String id, Status status) {
        if (userId == null) throw new EmptyUserIdException();
        if (id == null) throw new EmptyIdException();
        final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByName(final  String userId, String name, Status status) {
        if (userId == null) throw new EmptyUserIdException();
        if (name == null) throw new EmptyNameException();
        final Task task = findOneByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final  String userId, Integer index, Status status) {
        if (userId == null) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

}
