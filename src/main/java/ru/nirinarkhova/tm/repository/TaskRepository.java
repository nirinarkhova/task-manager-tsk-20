package ru.nirinarkhova.tm.repository;

import ru.nirinarkhova.tm.api.repository.ITaskRepository;
import ru.nirinarkhova.tm.exception.empty.EmptyUserIdException;
import ru.nirinarkhova.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAll(Comparator<Task> comparator) {
        final List<Task> tasks = new ArrayList<>(entities);
        tasks.sort(comparator);
        return tasks;
    }

    public Task findOneByIndex(final  String userId, final Integer index) {
        if (userId == null) throw new EmptyUserIdException();
        return entities.get(index);
    }

    @Override
    public Task findOneByName(final  String userId, final String name) {
        if (userId == null) throw new EmptyUserIdException();
        for (final Task task: entities) {
            if (!userId.equals(task.getUserId())) continue;
            if(name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByIndex(final  String userId, final Integer index) {
        if (userId == null) throw new EmptyUserIdException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeOneByName(final  String userId, final String name) {
        if (userId == null) throw new EmptyUserIdException();
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task findOneByProjectId(final  String userId, String projectId) {
        if (userId == null) throw new EmptyUserIdException();
        for (final Task task: entities) {
            if (!userId.equals(task.getUserId())) continue;
            if(projectId.equals(task.getProjectId())) return task;
        }
        return null;
    }

    @Override
    public Task bindTaskByProject(final  String userId, String taskId, String projectId) {
        if (userId == null) throw new EmptyUserIdException();
        if (projectId == null) return null;
        if (taskId == null) return null;
        final Task task = findById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final  String userId, String projectId) {
        if (userId == null) throw new EmptyUserIdException();
        final List<Task> projectTasks = new ArrayList<>();
        for (final Task task: entities){
            if (!userId.equals(task.getUserId())) continue;
            if (projectId.equals(task.getProjectId())) projectTasks.add(task);
        }
        return projectTasks;
    }

    @Override
    public void removeAllByProjectId(final  String userId, String projectId) {
        if (userId == null) throw new EmptyUserIdException();
        entities.removeIf(task -> projectId.equals(task.getProjectId()));
    }

    @Override
    public Task unbindTaskByProject(final  String userId, String taskId) {
        if (userId == null) throw new EmptyUserIdException();
        if (taskId == null) return null;
        final Task task = findById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

}

