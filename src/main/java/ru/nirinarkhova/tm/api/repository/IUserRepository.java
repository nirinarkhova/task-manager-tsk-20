package ru.nirinarkhova.tm.api.repository;

import ru.nirinarkhova.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String login);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String Login );

}
