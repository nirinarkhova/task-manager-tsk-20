package ru.nirinarkhova.tm.exception.system;

import ru.nirinarkhova.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(String command) {
        super("Error! Unknown ``" + command + "`` command...");
    }

}
