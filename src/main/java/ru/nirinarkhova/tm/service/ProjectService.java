package ru.nirinarkhova.tm.service;

import ru.nirinarkhova.tm.api.repository.IProjectRepository;
import ru.nirinarkhova.tm.api.service.IProjectService;
import ru.nirinarkhova.tm.enumerated.Status;
import ru.nirinarkhova.tm.exception.empty.EmptyIdException;
import ru.nirinarkhova.tm.exception.empty.EmptyIndexException;
import ru.nirinarkhova.tm.exception.empty.EmptyNameException;
import ru.nirinarkhova.tm.exception.empty.EmptyUserIdException;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return null;
        return projectRepository.findAll(comparator);
    }

    @Override
    public Project add(final  String userId, final String name, final String description) {
        if (userId == null) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return null;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
        return project;
    }

    @Override
    public Project findOneByName(final  String userId, final String name) {
        if (userId == null) throw new EmptyUserIdException();
        if (name == null) throw new EmptyNameException();
        return projectRepository.findOneByName(userId, name);
    }

    @Override
    public Project removeOneByName(final  String userId, final String name) {
        if (userId == null) throw new EmptyUserIdException();
        if (name == null) throw new EmptyNameException();
        return projectRepository.removeOneByName(userId, name);
    }

    @Override
    public Project findOneByIndex(final  String userId, final Integer index) {
        if (userId == null) throw new EmptyUserIdException();
        if (index == null) throw new EmptyIndexException();
        return projectRepository.findOneByIndex(userId, index);
    }

    @Override
    public Project removeOneByIndex(final  String userId, final Integer index) {
        if (userId == null) throw new EmptyUserIdException();
        if (index == null) throw new EmptyIndexException();
        return projectRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Project updateProjectByIndex(final  String userId, final Integer index, String name, String description) {
        if (userId == null) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectById(final  String userId, final String id, String name, String description) {
        if (userId == null) throw new EmptyUserIdException();
        if (id == null) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startProjectById(final  String userId, String id) {
        if (userId == null) throw new EmptyUserIdException();
        if (id == null) throw new EmptyIdException();
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByName(final  String userId, String name) {
        if (userId == null) throw new EmptyUserIdException();
        if (name == null) throw new EmptyNameException();
        final Project project = findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByIndex(final  String userId, Integer index) {
        if (userId == null) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishProjectById(final  String userId, String id) {
        if (userId == null) throw new EmptyUserIdException();
        if (id == null) throw new EmptyIdException();
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByName(final  String userId, String name) {
        if (userId == null) throw new EmptyUserIdException();
        if (name == null) throw new EmptyNameException();
        final Project project = findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByIndex(final  String userId, Integer index) {
        if (userId == null) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final  String userId, String id, Status status) {
        if (userId == null) throw new EmptyUserIdException();
        if (id == null) throw new EmptyIdException();
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByName(final  String userId, String name, Status status) {
        if (userId == null) throw new EmptyUserIdException();
        if (name == null) throw new EmptyNameException();
        final Project project = findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final  String userId, Integer index, Status status) {
        if (userId == null) throw new EmptyUserIdException();
        if (index == null || index < 0) return null;
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}
