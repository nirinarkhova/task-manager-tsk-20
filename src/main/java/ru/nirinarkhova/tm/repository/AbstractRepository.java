package ru.nirinarkhova.tm.repository;

import ru.nirinarkhova.tm.api.IRepository;
import ru.nirinarkhova.tm.exception.empty.EmptyIdException;
import ru.nirinarkhova.tm.exception.empty.EmptyUserIdException;
import ru.nirinarkhova.tm.exception.entity.ObjectNotFoundException;
import ru.nirinarkhova.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public List<E> findAll(final String userId) {
        final List<E> result = new ArrayList<>();
        for (final E entity : entities) {
            if (userId.equals(entity.getUserId())) result.add(entity);
        }
        return result;

    }

    @Override
    public E add(final String userId, final E entity) {
        if (userId == null) throw new EmptyUserIdException();
        entities.add(entity);
        return entity;
    }

    @Override
    public E findById(final String userId, final String id) {
        if (userId == null) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        for (final E entity: entities) {
            if (entity == null) continue;
            if (!userId.equals(entity.getUserId())) continue;
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public void clear(final String userId) {
        if (userId == null) throw new EmptyUserIdException();
        final List<E> result = findAll(userId);
        this.entities.removeAll(result);
    }

    @Override
    public E removeById(final String userId, final String id) {
        if (userId == null) throw new EmptyUserIdException();
        final E entity = findById(userId, id);
        if (entity == null) throw new ObjectNotFoundException();
        entities.remove(entity);
        return entity;
    }

    @Override
    public void remove(final String userId, final E entity) {
        if (userId == null) throw new EmptyUserIdException();
        entities.remove(entity);
    }

}

