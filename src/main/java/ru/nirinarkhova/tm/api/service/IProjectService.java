package ru.nirinarkhova.tm.api.service;

import ru.nirinarkhova.tm.api.IService;
import ru.nirinarkhova.tm.enumerated.Status;
import ru.nirinarkhova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IService<Project> {

    List<Project> findAll(Comparator<Project> comparator);

    Project add(String userId, String name, String description);

    Project findOneByIndex(String userId, Integer index);

    Project findOneByName(String userId, String name);

    Project removeOneByIndex(String userId, Integer index);

    Project removeOneByName(String userId, String name);

    Project updateProjectById(String userId, String id, String name, String description);

    Project updateProjectByIndex(String userId, Integer index, String name, String description);

    Project startProjectById(String userId, String id);

    Project startProjectByName(String userId, String name);

    Project startProjectByIndex(String userId, Integer index);

    Project finishProjectById(String userId, String id);

    Project finishProjectByName(String userId, String name);

    Project finishProjectByIndex(String userId, Integer index);

    Project changeProjectStatusById(String userId, String id, Status status);

    Project changeProjectStatusByName(String userId, String name, Status status);

    Project changeProjectStatusByIndex(String userId, Integer index, Status status);

}
