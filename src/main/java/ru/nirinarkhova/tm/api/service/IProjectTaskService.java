package ru.nirinarkhova.tm.api.service;

import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findTaskByProjectId(String userId, String projectId);

     Task bindTaskByProject(String userId, String taskId, String projectId);

     Task unbindTaskFromProject(String userId, String taskId);

     Project removeProjectById(String userId, String projectId);

}
