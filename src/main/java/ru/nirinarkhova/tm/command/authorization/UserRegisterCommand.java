package ru.nirinarkhova.tm.command.authorization;

import ru.nirinarkhova.tm.command.AbstractCommand;
import ru.nirinarkhova.tm.util.TerminalUtil;

public class UserRegisterCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-registration";
    }

    @Override
    public String description() {
        return "register a new user in task-manager.";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRATION]");
        System.out.println("[ENTER LOGIN:]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER EMAIL:]");
        final String email = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().register(login, password, email);
    }

}

