package ru.nirinarkhova.tm.command.system;

import ru.nirinarkhova.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String description() {
        return "show developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT:]");
        System.out.println("Name: NADEZHDA IRINARKHOVA");
        System.out.println("E-mail: nirinarkhova@tsconsulting.com");
        System.out.println("Company: TSC");
    }

}

