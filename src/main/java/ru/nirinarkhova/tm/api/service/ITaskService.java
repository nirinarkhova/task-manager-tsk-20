package ru.nirinarkhova.tm.api.service;

import ru.nirinarkhova.tm.api.IService;
import ru.nirinarkhova.tm.enumerated.Status;
import ru.nirinarkhova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IService<Task> {

    List<Task> findAll(Comparator<Task> comparator);

    Task add(String userId, String name, String description);

    Task findOneByIndex(String userId, Integer index);

    Task findOneByName(String userId, String name);

    Task removeOneByIndex(String userId, Integer index);

    Task removeOneByName(String userId, String name);

    Task updateTaskById(String userId, String id, String name, String description);

    Task updateTaskByIndex(String userId, Integer index, String name, String description);

    Task startTaskById(String userId, String id);

    Task startTaskByName(String userId, String name);

    Task startTaskByIndex(String userId, Integer index);

    Task finishTaskById(String userId, String id);

    Task finishTaskByName(String userId, String name);

    Task finishTaskByIndex(String userId, Integer index);

    Task changeTaskStatusById(String userId, String id, Status status);

    Task changeTaskStatusByName(String userId, String name, Status status);

    Task changeTaskStatusByIndex(String userId, Integer index, Status status);

}
