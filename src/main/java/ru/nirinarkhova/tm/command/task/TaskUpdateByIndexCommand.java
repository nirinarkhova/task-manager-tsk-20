package ru.nirinarkhova.tm.command.task;

import ru.nirinarkhova.tm.command.AbstractTaskCommand;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.model.Task;
import ru.nirinarkhova.tm.util.TerminalUtil;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Override
    public String description() {
        return "update a task by index.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER TASK INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("[ENTER TASK NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdate = serviceLocator.getTaskService().updateTaskByIndex(userId, index, name, description);
        if (taskUpdate == null) throw new TaskNotFoundException();
    }

}

